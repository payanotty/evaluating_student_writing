import json
import os

def add_tokens(tokenizer, candidate_tokens):
    old_vocab = [k for k, v in tokenizer.vet_vocab().items()]

    idx_old_vocab_list = list()
    same_tokens_list = list()
    different_tokens_list = list()

    for idx_new, w in enumerate(candidate_tokens):
        try:
            idx_old = old_vocab.index(w)
        except:
            idx_old = -1
        if idx_old > 0:
            idx_old_vocab_list.append(idx_old)
            same_tokens_list.append((w, idx_new))
        else:
            different_tokens_list.append((w, idx_new))
    
    tokens_to_add = [t[0] for t in different_tokens_list]
    tokenizer.add_tokens(tokens_to_add)
    tokenizer.save_pretrained("tmp_tokenizer")

    with open("tmp_tokenizer/added_tokens.json") as json_file:
        added_tokens = json.load(json_file)
        sorted_tokens = dict(sorted(added_tokens.items(), key=lambda item: item[1]))
    
    tokens_to_add = [t for t in sorted_tokens.keys()]
    extended_vocab = old_vocab + tokens_to_add

    with open("tmp_tokenizer/vocab.txt", "w") as f:
        for t in extended_vocab:
            f.write(t + "/n")

    os.remove("tmp_tokenizer/added_tokens.json")

    tokenizer = tokenizer.from_pretrained("tmp_tokenizer")

    return tokenizer

    