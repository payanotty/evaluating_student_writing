import os
import random
import warnings
import pytorch_lightning as ptl
import tez
from tqdm import tqdm
from torch.utils.data import Dataset, DataLoader
import numpy as np
import pandas as pd
import torch
import torch.nn as nn
from sklearn import metrics
from torch.nn import functional as F
from transformers import (
    AdamW,
    AutoConfig,
    AutoModel,
    AutoTokenizer,
    get_cosine_schedule_with_warmup,
)
from omegaconf import DictConfig, OmegaConf
import hydra

from utils import (
    EarlyStopping,
    prepare_training_data,
    target_id_map,
    id_target_map,
    MlflowLogger,
    score_feedback_comp,
)
from writer import MlflowWriter

warnings.filterwarnings("ignore")

NUM_JOBS = 12


def seed_everything(seed: int):
    random.seed(seed)
    os.environ["PYTHONHASHSEED"] = str(seed)
    np.random.seed(seed)
    torch.manual_seed(seed)
    torch.cuda.manual_seed(seed)
    torch.backends.cudnn.deterministic = True
    torch.backends.cudnn.benchmark = True


class FeedbackDatasetValid:
    def __init__(self, samples, max_len, tokenizer):
        self.samples = samples
        self.max_len = max_len
        self.tokenizer = tokenizer
        self.length = len(samples)

    def __len__(self):
        return self.length

    def __getitem__(self, idx):
        input_ids = self.samples[idx]["input_ids"]
        input_ids = [self.tokenizer.cls_token_id] + input_ids

        if len(input_ids) > self.max_len - 1:
            input_ids = input_ids[: self.max_len - 1]

        # add end token id to the input_ids
        input_ids = input_ids + [self.tokenizer.sep_token_id]
        attention_mask = [1] * len(input_ids)

        return {
            "ids": input_ids,
            "mask": attention_mask,
        }


class Collate:
    def __init__(self, tokenizer):
        self.tokenizer = tokenizer

    def __call__(self, batch):
        output = dict()
        output["ids"] = [sample["ids"] for sample in batch]
        output["mask"] = [sample["mask"] for sample in batch]

        # calculate max token length of this batch
        batch_max = max([len(ids) for ids in output["ids"]])

        # add padding
        if self.tokenizer.padding_side == "right":
            output["ids"] = [
                s + (batch_max - len(s)) * [self.tokenizer.pad_token_id]
                for s in output["ids"]
            ]
            output["mask"] = [s + (batch_max - len(s)) * [0] for s in output["mask"]]
        else:
            output["ids"] = [
                (batch_max - len(s)) * [self.tokenizer.pad_token_id] + s
                for s in output["ids"]
            ]
            output["mask"] = [(batch_max - len(s)) * [0] + s for s in output["mask"]]

        # convert to tensors
        output["ids"] = torch.tensor(output["ids"], dtype=torch.long)
        output["mask"] = torch.tensor(output["mask"], dtype=torch.long)

        return output


class FeedbackDataset:
    def __init__(self, samples, max_len, tokenizer):
        self.samples = samples
        self.max_len = max_len
        self.tokenizer = tokenizer
        self.length = len(samples)

    def __len__(self):
        return self.length

    def __getitem__(self, idx):
        input_ids = self.samples[idx]["input_ids"]
        input_labels = self.samples[idx]["input_labels"]
        input_labels = [target_id_map[x] for x in input_labels]
        other_label_id = target_id_map["O"]
        padding_label_id = target_id_map["PAD"]
        # print(input_ids)
        # print(input_labels)

        # add start token id to the input_ids
        input_ids = [self.tokenizer.cls_token_id] + input_ids
        input_labels = [other_label_id] + input_labels

        if len(input_ids) > self.max_len - 1:
            input_ids = input_ids[: self.max_len - 1]
            input_labels = input_labels[: self.max_len - 1]

        # add end token id to the input_ids
        input_ids = input_ids + [self.tokenizer.sep_token_id]
        input_labels = input_labels + [other_label_id]

        attention_mask = [1] * len(input_ids)

        padding_length = self.max_len - len(input_ids)
        if padding_length > 0:
            if self.tokenizer.padding_side == "right":
                input_ids = input_ids + [self.tokenizer.pad_token_id] * padding_length
                input_labels = input_labels + [padding_label_id] * padding_length
                attention_mask = attention_mask + [0] * padding_length
            else:
                input_ids = [self.tokenizer.pad_token_id] * padding_length + input_ids
                input_labels = [padding_label_id] * padding_length + input_labels
                attention_mask = [0] * padding_length + attention_mask

        return {
            "ids": torch.tensor(input_ids, dtype=torch.long),
            "mask": torch.tensor(attention_mask, dtype=torch.long),
            "targets": torch.tensor(input_labels, dtype=torch.long),
        }


class FeedbackModelForPL(ptl.LightningModule):
    def __init__(
        self,
        model_name,
        num_train_steps,
        learning_rate,
        num_labels,
        steps_per_epoch,
        tokenizer,
        train_dataset,
        valid_dataset,
        train_batch_size,
        valid_batch_size,
        writer,
        cfg,
        fold,
    ):
        super().__init__()
        self.learning_rate = learning_rate
        self.model_name = model_name
        self.num_train_steps = num_train_steps
        self.num_labels = num_labels
        self.steps_per_epoch = steps_per_epoch
        self.step_scheduler_after = "batch"

        hidden_dropout_prob: float = 0.1
        layer_norm_eps: float = 1e-7

        config = AutoConfig.from_pretrained(model_name)

        config.update(
            {
                "output_hidden_states": True,
                "hidden_dropout_prob": hidden_dropout_prob,
                "layer_norm_eps": layer_norm_eps,
                "add_pooling_layer": False,
                "num_labels": self.num_labels,
            }
        )
        self.transformer = AutoModel.from_pretrained(model_name, config=config)
        self.dropout = nn.Dropout(config.hidden_dropout_prob)
        self.dropout1 = nn.Dropout(0.1)
        self.dropout2 = nn.Dropout(0.2)
        self.dropout3 = nn.Dropout(0.3)
        self.dropout4 = nn.Dropout(0.4)
        self.dropout5 = nn.Dropout(0.5)
        self.output = nn.Linear(config.hidden_size, self.num_labels)

        self.train_dataset = train_dataset
        self.valid_dataset = valid_dataset
        self.train_batch_size = train_batch_size
        self.valid_batch_size = valid_batch_size
        self.tokenizer = tokenizer
        self.writer = writer
        self.cfg = cfg
        self.fold = fold

        self.step_count = 0
        self.train_losses = []
        self.valid_losses = []
        self.train_f1s = []
        self.valid_f1s = []

    def monitor_metrics(self, outputs, targets, attention_mask):
        active_loss = (attention_mask.view(-1) == 1).cpu().numpy()
        active_logits = outputs.view(-1, self.num_labels)
        true_labels = targets.view(-1).cpu().numpy()
        outputs = active_logits.argmax(dim=-1).cpu().numpy()
        idxs = np.where(active_loss == 1)[0]
        f1_score = metrics.f1_score(true_labels[idxs], outputs[idxs], average="macro")
        return {"f1": f1_score}

    def forward(self, ids, mask, token_type_ids=None, targets=None):

        if token_type_ids:
            transformer_out = self.transformer(ids, mask, token_type_ids)
        else:
            transformer_out = self.transformer(ids, mask)
        sequence_output = transformer_out.last_hidden_state
        sequence_output = self.dropout(sequence_output)

        logits1 = self.output(self.dropout1(sequence_output))
        logits2 = self.output(self.dropout2(sequence_output))
        logits3 = self.output(self.dropout3(sequence_output))
        logits4 = self.output(self.dropout4(sequence_output))
        logits5 = self.output(self.dropout5(sequence_output))

        logits = (logits1 + logits2 + logits3 + logits4 + logits5) / 5
        logits = torch.softmax(logits, dim=-1)
        loss = 0

        if targets is not None:
            loss1 = self.loss(logits1, targets, attention_mask=mask)
            loss2 = self.loss(logits2, targets, attention_mask=mask)
            loss3 = self.loss(logits3, targets, attention_mask=mask)
            loss4 = self.loss(logits4, targets, attention_mask=mask)
            loss5 = self.loss(logits5, targets, attention_mask=mask)
            loss = (loss1 + loss2 + loss3 + loss4 + loss5) / 5
            f1_1 = self.monitor_metrics(logits1, targets, attention_mask=mask)["f1"]
            f1_2 = self.monitor_metrics(logits2, targets, attention_mask=mask)["f1"]
            f1_3 = self.monitor_metrics(logits3, targets, attention_mask=mask)["f1"]
            f1_4 = self.monitor_metrics(logits4, targets, attention_mask=mask)["f1"]
            f1_5 = self.monitor_metrics(logits5, targets, attention_mask=mask)["f1"]
            f1 = (f1_1 + f1_2 + f1_3 + f1_4 + f1_5) / 5
            metric = {"f1": f1}
            return logits, loss, metric

        return logits, loss, {}

    def predict_one_step(self, data):
        output, _, _ = self(**data)
        return output

    def process_output(self, output):
        output = output.cpu().detach().numpy()
        return output

    def predict(self, dataset, sampler=None, batch_size=16, n_jobs=1, collate_fn=None):
        if next(self.parameters()).device != self.device:
            self.to(self.device)

        if batch_size == 1:
            n_jobs = 0
        data_loader = torch.utils.data.DataLoader(
            dataset,
            batch_size=batch_size,
            num_workers=n_jobs,
            sampler=sampler,
            collate_fn=collate_fn,
            pin_memory=True,
        )

        if self.training:
            self.eval()

        if self.using_tpu:
            tk0 = data_loader
        else:
            tk0 = tqdm(data_loader, total=len(data_loader))

        for _, data in enumerate(tk0):
            with torch.no_grad():
                out = self.predict_one_step(data)
                out = self.process_output(out)
                yield out

            if not self.using_tpu:
                tk0.set_postfix(stage="test")

        if not self.using_tpu:
            tk0.close()

    def loss(self, outputs, targets, attention_mask):
        loss_fct = nn.CrossEntropyLoss()

        active_loss = attention_mask.view(-1) == 1
        active_logits = outputs.view(-1, self.num_labels)
        true_labels = targets.view(-1)
        outputs = active_logits.argmax(dim=-1)
        idxs = np.where(active_loss.cpu().numpy() == 1)[0]
        active_logits = active_logits[idxs]
        true_labels = true_labels[idxs].to(torch.long)

        loss = loss_fct(active_logits, true_labels)
        return loss

    def configure_optimizers(self):
        param_optimizer = list(self.named_parameters())
        no_decay = ["bias", "LayerNorm.bias"]
        optimizer_parameters = [
            {
                "params": [
                    p for n, p in param_optimizer if not any(nd in n for nd in no_decay)
                ],
                "weight_decay": 0.01,
            },
            {
                "params": [
                    p for n, p in param_optimizer if any(nd in n for nd in no_decay)
                ],
                "weight_decay": 0.0,
            },
        ]
        opt = AdamW(optimizer_parameters, lr=self.learning_rate)

        sch = get_cosine_schedule_with_warmup(
            opt,
            num_warmup_steps=int(0.1 * self.num_train_steps),
            num_training_steps=self.num_train_steps,
            num_cycles=1,
            last_epoch=-1,
        )

        return [opt], [{"scheduler": sch, "interval": "step"}]

    def train_dataloader(self):
        # training_samples = prepare_training_data(
        #     self.train_df, self.tokenizer, self.cfg, num_jobs=NUM_JOBS
        # )
        # train_dataset = FeedbackDataset(training_samples, self.cfg.max_len, self.tokenizer)
        return DataLoader(
            self.train_dataset,
            batch_size=self.train_batch_size,
            pin_memory=True,
            shuffle=True,
            num_workers=8,
        )

    def val_dataloader(self):
        # valid_samples = prepare_training_data(self.valid_df, self.tokenizer, self.cfg, num_jobs=NUM_JOBS)
        # valid_dataset = FeedbackDatasetValid(valid_samples, max_len=4096, tokenizer=self.tokenizer)
        return DataLoader(
            self.valid_dataset,
            batch_size=self.valid_batch_size,
            pin_memory=True,
            shuffle=True,
            num_workers=8,
        )

    def training_step(self, batch, batch_nb):
        logits, loss, metric = self.forward(**batch)

        loss_detach = float(self.process_output(loss))
        self.train_losses.append(loss_detach)
        self.train_f1s.append(metric["f1"])
        # if len(self.train_losses)%self.steps_per_epoch==0:
        #     epoch_loss = np.mean(self.train_losses[-self.steps_per_epoch:])
        #     epoch_f1 = np.mean(self.train_f1s[-self.steps_per_epoch:])
        #     self.writer.log_metric(f"train_loss_per_epochs_{self.fold}", epoch_loss)
        #     self.writer.log_metric(f"train_f1_per_epochs_{self.fold}", epoch_f1)

        if len(self.train_losses) % 500 == 0:
            self.writer.log_metric(f"train_loss_per_steps_{self.fold}", loss_detach)
            self.writer.log_metric(f"train_f1_per_steps_{self.fold}", metric["f1"])
        return {"loss": loss, "f1": metric}

    def validation_step(self, batch, batch_nb):
        logits, loss, metric = self.forward(**batch)
        loss_detach = float(self.process_output(loss))
        self.valid_losses.append(loss_detach)
        self.valid_f1s.append(metric["f1"])
        # if len(self.valid_losses)%self.steps_per_epoch==0:
        #     epoch_loss = np.mean(self.valid_losses[-self.steps_per_epoch:])
        #     epoch_f1 = np.mean(self.valid_f1s[-self.steps_per_epoch:])
        #     self.writer.log_metric(f"valid_loss_per_epochs_{self.fold}", epoch_loss)
        #     self.writer.log_metric(f"valid_f1_per_epochs_{self.fold}", epoch_f1)

        # if len(self.valid_losses)%500==0:
        #     self.writer.log_metric(f"val_loss_per_steps_{self.fold}", loss_detach)
        #     self.writer.log_metric(f"val_f1_per_steps_{self.fold}", metric["f1"])
        return {"loss": loss, "f1": metric}

    def training_epoch_end(self, outputs):
        epoch_loss = np.mean(self.train_losses)
        epoch_f1 = np.mean(self.train_f1s)
        self.writer.log_metric(f"train_loss_per_epochs_{self.fold}", epoch_loss)
        self.writer.log_metric(f"train_f1_per_epochs_{self.fold}", epoch_f1)

        self.train_losses = []
        self.train_f1s = []
        # return {"avg_train_loss": epoch_loss, "avg_train_f1": epoch_f1}

    def validation_epoch_end(self, outputs):
        avg_loss = np.mean(self.valid_losses)
        avg_f1 = np.mean(self.valid_f1s)
        # avg_loss = torch.stack([x['loss'] for x in outputs]).mean().detach().cpu().item()
        # avg_f1 = torch.stack([x['f1'] for x in outputs]).mean().detach().cpu().item()
        self.writer.log_metric(f"val_loss_{self.fold}", avg_loss)
        self.writer.log_metric(f"val_f1_{self.fold}", avg_f1)

        self.valid_losses = []
        self.valid_f1s = []
        return {"avg_val_loss": avg_loss, "avg_val_f1": avg_f1}


class FeedbackModel(tez.Model):
    def __init__(self, model_name, num_labels):
        super().__init__()
        self.model_name = model_name
        self.num_labels = num_labels
        config = AutoConfig.from_pretrained(model_name)

        hidden_dropout_prob: float = 0.1
        layer_norm_eps: float = 1e-7
        config.update(
            {
                "output_hidden_states": True,
                "hidden_dropout_prob": hidden_dropout_prob,
                "layer_norm_eps": layer_norm_eps,
                "add_pooling_layer": False,
            }
        )
        self.transformer = AutoModel.from_config(config)
        self.output = nn.Linear(config.hidden_size, self.num_labels)

    def forward(self, ids, mask):
        transformer_out = self.transformer(ids, mask)
        sequence_output = transformer_out.last_hidden_state
        logits = self.output(sequence_output)
        logits = torch.softmax(logits, dim=-1)
        return logits, 0, {}


def create_fold_dataset(cfg, df, fold):
    print("now tokenizing training and validation data...")
    train_df = df[df["kfold"] != fold].reset_index(drop=True)
    valid_df = df[df["kfold"] == fold].reset_index(drop=True)

    tokenizer = AutoTokenizer.from_pretrained(cfg.model)

    training_samples = prepare_training_data(
        train_df, tokenizer, cfg, num_jobs=NUM_JOBS
    )
    train_dataset = FeedbackDataset(training_samples, cfg.max_len, tokenizer)

    valid_samples = prepare_training_data(valid_df, tokenizer, cfg, num_jobs=NUM_JOBS)

    valid_dataset1 = FeedbackDataset(valid_samples, max_len=4096, tokenizer=tokenizer)
    valid_dataset2 = FeedbackDatasetValid(
        valid_samples, max_len=4096, tokenizer=tokenizer
    )

    return train_dataset, valid_dataset1, valid_dataset2, tokenizer


def validation_in_the_end(model, tokenizer, valid_dataset, valid_df):
    model.eval()
    valid_samples = valid_dataset.samples
    collate = Collate(tokenizer)

    preds_iter = model.predict(
        valid_dataset,
        batch_size=4,
        n_jobs=-1,
        collate_fn=collate,
    )

    final_preds = []
    final_scores = []
    for preds in preds_iter:
        pred_class = np.argmax(preds, axis=2)
        pred_scrs = np.max(preds, axis=2)
        for pred, pred_scr in zip(pred_class, pred_scrs):
            final_preds.append(pred.tolist())
            final_scores.append(pred_scr.tolist())

    for j in range(len(valid_samples)):
        tt = [id_target_map[p] for p in final_preds[j][1:]]
        tt_score = final_scores[j][1:]
        valid_samples[j]["preds"] = tt
        valid_samples[j]["pred_scores"] = tt_score

    submission = []
    min_thresh = {
        "Lead": 9,
        "Position": 5,
        "Evidence": 14,
        "Claim": 3,
        "Concluding Statement": 11,
        "Counterclaim": 6,
        "Rebuttal": 4,
    }
    proba_thresh = {
        "Lead": 0.7,
        "Position": 0.55,
        "Evidence": 0.65,
        "Claim": 0.55,
        "Concluding Statement": 0.7,
        "Counterclaim": 0.5,
        "Rebuttal": 0.55,
    }

    for _, sample in enumerate(valid_samples):
        preds = sample["preds"]
        offset_mapping = sample["offset_mapping"]
        sample_id = sample["id"]
        sample_text = sample["text"]
        sample_pred_scores = sample["pred_scores"]

        # pad preds to same length as offset_mapping
        if len(preds) < len(offset_mapping):
            preds = preds + ["O"] * (len(offset_mapping) - len(preds))
            sample_pred_scores = sample_pred_scores + [0] * (
                len(offset_mapping) - len(sample_pred_scores)
            )

        idx = 0
        phrase_preds = []
        while idx < len(offset_mapping):
            start, _ = offset_mapping[idx]
            if preds[idx] != "O":
                label = preds[idx][2:]
            else:
                label = "O"
            phrase_scores = []
            phrase_scores.append(sample_pred_scores[idx])
            idx += 1
            while idx < len(offset_mapping):
                if label == "O":
                    matching_label = "O"
                else:
                    matching_label = f"I-{label}"
                if preds[idx] == matching_label:
                    _, end = offset_mapping[idx]
                    phrase_scores.append(sample_pred_scores[idx])
                    idx += 1
                else:
                    break
            if "end" in locals():
                phrase = sample_text[start:end]
                phrase_preds.append((phrase, start, end, label, phrase_scores))

        temp_df = []
        for phrase_idx, (phrase, start, end, label, phrase_scores) in enumerate(
            phrase_preds
        ):
            word_start = len(sample_text[:start].split())
            word_end = word_start + len(sample_text[start:end].split())
            word_end = min(word_end, len(sample_text.split()))
            ps = " ".join([str(x) for x in range(word_start, word_end)])
            if label != "O":
                if sum(phrase_scores) / len(phrase_scores) >= proba_thresh[label]:
                    temp_df.append((sample_id, label, ps))

        temp_df = pd.DataFrame(temp_df, columns=["id", "class", "predictionstring"])

        submission.append(temp_df)

    submission = pd.concat(submission).reset_index(drop=True)
    submission["len"] = submission.predictionstring.apply(lambda x: len(x.split()))

    def threshold(df):
        df = df.copy()
        for key, value in min_thresh.items():
            index = df.loc[df["class"] == key].query(f"len<{value}").index
            df.drop(index, inplace=True)
        return df

    submission = threshold(submission)

    # drop len
    submission = submission.drop(columns=["len"])

    scr = score_feedback_comp(submission, valid_df, return_class_scores=True)
    print(scr)

    return scr


def run_fold(cfg, df, fold, writer):
    valid_df = df[df["kfold"] == fold].reset_index(drop=True)
    train_dataset, valid_dataset1, valid_dataset2, tokenizer = create_fold_dataset(
        cfg, df, fold
    )
    num_train_steps = int(
        len(train_dataset) / cfg.batch_size / cfg.accumulation_steps * cfg.epochs
    )

    print(f"train size: {len(train_dataset)}")
    print(f"num train steps: {num_train_steps}")

    model = FeedbackModelForPL(
        model_name=cfg.model,
        num_train_steps=num_train_steps,
        learning_rate=cfg.lr,
        num_labels=len(target_id_map) - 1,
        steps_per_epoch=len(train_dataset) / cfg.batch_size,
        tokenizer=tokenizer,
        train_dataset=train_dataset,
        valid_dataset=valid_dataset1,
        train_batch_size=cfg.batch_size,
        valid_batch_size=cfg.batch_size,
        writer=writer,
        cfg=cfg,
        fold=fold,
    )

    trainer = ptl.Trainer(
        num_sanity_val_steps=0,
        gpus=torch.cuda.device_count(),
        strategy="dp",
        num_nodes=1,
        max_epochs=cfg.epochs,
        accumulate_grad_batches=cfg.accumulation_steps,
        precision=16,
        amp_backend="native",
    )

    trainer.fit(model)
    save_dir = os.path.join(cfg.output, f"model_{fold}.ckpt")
    # torch.save(model.model.state_dict(), save_dir)
    trainer.save_checkpoint(save_dir)

    tez_model = FeedbackModel(model_name=cfg.model, num_labels=len(target_id_map) - 1)
    state_dict = torch.load(save_dir)["state_dict"]

    tez_model.load_state_dict(state_dict)
    tez_save_dir = os.path.join(cfg.output, f"model_{fold}.bin")
    tez_model.save(tez_save_dir, weights_only=True)

    scr = validation_in_the_end(tez_model, tokenizer, valid_dataset2, valid_df)
    f1 = scr[0]
    summary = scr[1]
    writer.log_metric(f"valid_f1_{fold}", scr[0])

    for n, v in summary.items():
        writer.log_metric(f"valid_{n}_{fold}", v)
    print("model saving and loading is successed!")
    # print(f"final validation f1 is {scr}")


@hydra.main(config_path="../config", config_name="config")
def main(cfg: DictConfig):
    orig_dir = hydra.utils.get_original_cwd()
    os.chdir(orig_dir)
    print(f"Original working directry: {orig_dir}")
    writer = MlflowWriter(cfg.experiment_name)

    writer.log_param("model", cfg.model)
    writer.log_param("lr", cfg.lr)
    writer.log_param("max_len", cfg.max_len)
    writer.log_param("batch_size", cfg.batch_size)
    writer.log_param("accumulation_steps", cfg.accumulation_steps)
    writer.log_param("epochs", cfg.epochs)

    seed_everything(42)
    os.makedirs(cfg.output, exist_ok=True)
    df = pd.read_csv(os.path.join(cfg.input, "train_folds.csv"))

    # 検証用
    # df = df[:1000]

    for fold in range(cfg.folds):
        run_fold(cfg, df, fold, writer)

    writer.log_artifacts(cfg.output)
    writer.set_terminated()


if __name__ == "__main__":
    main()
