# evaluating_student_writing

## コンペ概要

https://www.kaggle.com/c/feedback-prize-2021/overview


## データセットダウンロード

1. Kaggleのアカウント画面から```kaggle.json```をダウンロード
2. ```~/.kaggle```以下に```kaggle.json```を配置
3. kaggle apiが使えるようになるので、コンペのDataページのコマンドをコピペして実行

## training実行
```
docker-compose up
```