FROM gcr.io/kaggle-gpu-images/python:latest

WORKDIR /

# RUN set -xe \
#     && apt-get update \
#     && apt-get -y install python-pip
# RUN pip install --upgrade pip
RUN pip install mlflow
RUN pip install hydra-core --upgrade
RUN pip install omegaconf
RUN pip install tez